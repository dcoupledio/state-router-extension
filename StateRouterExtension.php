<?php namespace Decoupled\Core\Extension\State;

use Decoupled\Core\State\StateRouter;
use Decoupled\Core\State\StateRouteFactory;
use Decoupled\Core\State\StateRouteCollection;
use Decoupled\Core\State\StateRouteResolver;
use Decoupled\Core\State\StateParser;
use Decoupled\Core\Application\ApplicationExtension;

class StateRouterExtension extends ApplicationExtension{

    public function getName()
    {
        return 'state.router.extension';
    }

    public function extend()
    {
        $app = $this->getApp();

        $app['$state.parser'] = function(){

            return new StateParser();
        };

        $app['$state.route.factory'] = function( $container ){

            $factory = new StateRouteFactory();

            $factory->setActionFactory( $container['$action.factory'] );

            $factory->setParser( $container['$state.parser'] );

            return $factory;
        };  

        $app['$state.route.collection'] = function(){

            return new StateRouteCollection();
        };

        $app['$state.route.resolver'] = function(){

            return new StateRouteResolver();
        };

        $app['$state.router'] = function( $container ){

            $router = new StateRouter();

            $router->setCollection( $container['$state.route.collection'] );

            $router->setRouteFactory( $container['$state.route.factory'] );

            $router->setResolver( $container['$state.route.resolver'] );

            return $router;
        };

        $app->setMethod('state', function() use($app){

            $router = $app['$state.router'];

            $assignState = [ $router, 'get' ];

            $params = func_get_args();

            if( count($params) > 0 )
                return call_user_func_array( $assignState, $params );

            return $router;
        });
    }
}