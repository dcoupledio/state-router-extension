<?php

require('../vendor/autoload.php');

use phpunit\framework\TestCase;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Application\ApplicationContainer;
use Decoupled\Core\Extension\Action\ActionExtension;
use Decoupled\Core\Extension\State\StateRouterExtension;
use Decoupled\Core\State\StateRoute;

class ExtTest extends TestCase{

    public function testAppCanUseRouter()
    {
        $app = new Application( new ApplicationContainer() );

        $app->uses( new ActionExtension() );

        $app->uses( new StateRouterExtension() );

        return $app;
    }

    /**
     * @depends testAppCanUseRouter
     */

    public function testCanAssignStatesViaAppMethod( $app )
    {
        $app->state('example.state')
            ->when('state1 state2')
            ->uses(function(){ return 1; });

        $route = $app['$state.route.collection']->get('example.state');

        $this->assertInstanceOf( StateRoute::class, $route );  

        $this->assertContains( 'state1', $route->getStates() );  

        return $app; 
    }

    /**
     * @depends testCanAssignStatesViaAppMethod
     */

    public function testCanResolveViaAppMethod( $app )
    {
        $actions = $app->state()->resolve(['state1', 'state2']);

        $this->assertEquals( count($actions), 1 );

        $this->assertEquals( $actions[0](), 1 );
    }
}